/**
 * 
 */
package br.com.massuda.alexander.crm.api.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.crm.api.dao.DAO;
import br.com.massuda.alexander.crm.api.dao.Finder;
import br.com.massuda.alexander.crm.api.model.tarefas.Prioridade;
import br.com.massuda.alexander.crm.api.servico.ServicoUsuario;
import br.com.waiso.framework.exceptions.ErroUsuario;

/**
 * @author Alex
 *
 */
@Controller
@RequestMapping("/prioridade")
public class PrioridadeController {
	
	@Autowired
	ServicoUsuario servicoUsuario;
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@Valid @RequestBody Prioridade o) throws ErroUsuario, SQLException {
		DAO<Prioridade> dao = new DAO<Prioridade>(Prioridade.class) {};
		dao.incluir(o);
	}
	
	@ResponseBody
	@GetMapping
	public List<Prioridade> listar() {
		Finder<Prioridade> finder = new Finder<Prioridade>(Prioridade.class) {};
		return finder.listar();
	}
	
	@ResponseBody
	@GetMapping("/{id}")
	public Prioridade pesquisar(@PathVariable long id) {
		Finder<Prioridade> finder = new Finder<Prioridade>(Prioridade.class) {};
		Prioridade o = finder.pesquisar(id);
		return o;
	}
	
}
