/**
 * 
 */
package br.com.massuda.alexander.crm.api.model.tarefas;

/**
 * @author Alex
 *
 */
public enum StatusTarefa {

	REALIZADO(null),
	EM_ANDAMENTO(REALIZADO),
	DESIGNADO(EM_ANDAMENTO),
	ABERTO(DESIGNADO);
	
	private StatusTarefa proximo;

	private StatusTarefa(StatusTarefa proximo) {
		this.proximo = proximo;
	}

	public StatusTarefa getProximo() {
		return proximo;
	}
	
}
