package br.com.massuda.alexander.crm.api.framework.model;

import java.util.HashMap;
import java.util.Map;

public class SessoesDeUsuario {

	private static final Map<String, SessaoDeUsuario> sessoes = new HashMap<>();
	
	public SessaoDeUsuario get(String login) {
		return sessoes.get(login);
	}
	
	public void add (String login, SessaoDeUsuario sessaoDeUsuario) {
		sessoes.put(login, sessaoDeUsuario);
	}
	
	public boolean temSessaoAtiva (String login) {
		return get(login) != null;
	}
	
	public void deslogar (String login) {
		if (temSessaoAtiva(login)) {
			sessoes.remove(login);
		}
	}
	
}
