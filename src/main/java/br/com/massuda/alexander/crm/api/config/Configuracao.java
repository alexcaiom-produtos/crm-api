/**
 * 
 */
package br.com.massuda.alexander.crm.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Alex
 *
 */
@Configuration
@Import({
	br.com.massuda.alexander.spring.framework.infra.web.config.Configuracao.class
	})
public class Configuracao {

}
