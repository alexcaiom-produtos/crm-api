package br.com.massuda.alexander.crm.api.utils;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.massuda.alexander.crm.api.framework.model.SessaoDeUsuario;
import br.com.massuda.alexander.crm.api.framework.model.SessoesDeUsuario;

/**
 * @author Alex
 *
 */
@Component
public class GerenciamentoDeSessoesDeUsuario {
	
	private static final String SESSOES_USUARIO = "sessoesUsuario";
	@Autowired
	private HttpSession httpSession;

	public boolean getStatusLogon(String usuario) {
		SessoesDeUsuario sessoesUsuarios = getSessoesDeUsuario();
		boolean temSessaoAtiva = sessoesUsuarios.temSessaoAtiva(usuario);
		if (temSessaoAtiva) {
			boolean sessaoValida = validaTempoDeConexao(usuario);
			return sessaoValida;
		}
		return temSessaoAtiva;
	}
	
	public void addSessaoUsuario(String login, SessaoDeUsuario sessaoDeUsuario) {
		SessoesDeUsuario sessoesDeUsuario = getSessoesDeUsuario();
		sessoesDeUsuario.add(login, sessaoDeUsuario);
		httpSession.setAttribute(SESSOES_USUARIO, sessoesDeUsuario);
	}
	
	public void removeSessaoUsuario(String login) {
		SessoesDeUsuario sessoesDeUsuario = getSessoesDeUsuario();
		sessoesDeUsuario.deslogar(login);
		httpSession.setAttribute(SESSOES_USUARIO, sessoesDeUsuario);
	}
	
	private SessoesDeUsuario getSessoesDeUsuario () {
		SessoesDeUsuario sessoesUsuarios = (SessoesDeUsuario) httpSession.getAttribute(SESSOES_USUARIO);
		if (sessoesUsuarios == null) {
			sessoesUsuarios = new SessoesDeUsuario();
			httpSession.setAttribute(SESSOES_USUARIO, sessoesUsuarios);
		}
		return sessoesUsuarios;
	}

	private boolean validaTempoDeConexao(String usuario) {
		
		return true;
	}
	
}
