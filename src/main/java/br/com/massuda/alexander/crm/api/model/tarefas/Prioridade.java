package br.com.massuda.alexander.crm.api.model.tarefas;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Coluna;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;

public class Prioridade extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8313195726394302304L;
	@Coluna(nome = "descricao", unica=true)
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
