/**
 * 
 */
package br.com.massuda.alexander.crm.api.dao;

import br.com.massuda.alexander.crm.api.utils.ConstantesPersistencia;
import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;

/**
 * @author Alex
 *
 */
public class DAO<T> extends DAOGenericoJDBCImpl<T> {

	public DAO(Class<T> entidade) {
		super(entidade);
		configurarBancoDeDadosDadosAcesso();
	}

	/**
	 * 
	 */
	public static void configurarBancoDeDadosDadosAcesso() {
		br.com.massuda.alexander.persistencia.jdbc.utils.ConstantesPersistencia.BD_CONEXAO_LOCAL = ConstantesPersistencia.BANCO_DE_DADOS_LOCAL;
		br.com.massuda.alexander.persistencia.jdbc.utils.ConstantesPersistencia.BD_CONEXAO_USUARIO = ConstantesPersistencia.BANCO_DE_DADOS_CONEXAO_USUARIO;
		br.com.massuda.alexander.persistencia.jdbc.utils.ConstantesPersistencia.BD_CONEXAO_SENHA = ConstantesPersistencia.BANCO_DE_DADOS_CONEXAO_SENHA;
		br.com.massuda.alexander.persistencia.jdbc.utils.ConstantesPersistencia.BD_CONEXAO_NOME_BD = ConstantesPersistencia.BANCO_DE_DADOS_NOME;
		
		
		if (ConstantesPersistencia.BANCO_DE_DADOS_POOL_CONEXOES_ATIVO) {
			br.com.massuda.alexander.persistencia.jdbc.utils.ConstantesPersistencia.BANCO_DE_DADOS_POOL_CONEXOES_ATIVO = 
					ConstantesPersistencia.BANCO_DE_DADOS_POOL_CONEXOES_ATIVO;
		}
	}
	
}
