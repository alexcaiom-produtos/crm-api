package br.com.massuda.alexander.crm.api.servico;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.spring.framework.infra.web.servico.Servico;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

@Service
public class ServicoUsuario extends Servico<Usuario> {

	@Value("${api.autenticacao.servidor}")
	private String servidor;
	@Value("${api.autenticacao.porta}")
	private String porta;
	@Value("${api.autenticacao.servico}")
	private String contexto;
	private String url;
	
	@PostConstruct
	public void postInit() {
		url = new StringBuilder()
				.append("http://").append(servidor).append(":").append(porta)
				.append(contexto).append("/").append(entidade).toString();
//		target = client.target(url);
		tipo = Usuario.class;
	}

	public void editar(Usuario usuarioRecuperadoDoBD) throws Erro {
		restConector.postForEntity(url, usuarioRecuperadoDoBD);
//		target.request()
//		.post(Entity.entity(usuarioRecuperadoDoBD, MediaType.APPLICATION_JSON_TYPE));
	}
	
	public Usuario pesquisarPorLogin(String login) {
		StringBuilder urlTemp = new StringBuilder(url.toString());
		urlTemp.append("/login/").append(login);
		Usuario usuario = restConector.getForObject(urlTemp.toString());
//		Response resposta = target
//									.path("pesquisarPorLogin")
//									.queryParam("login", login)
//									.request().get();
//		Usuario usuario = getEntity(resposta);
		return usuario;
	}
	
	public Usuario autenticar(String login, String senha) throws Erro {
		StringBuilder urlTemp = new StringBuilder(url.toString());
		urlTemp.append("/").append("autenticar");
		Usuario u = new Usuario();
		u.setLogin(login);
		u.setSenha(senha);
//		Usuario usuario = restConector.getForObject(urlTemp.toString());
		Usuario usuario = restConector.postForEntity(urlTemp.toString(), u, Usuario.class);
//		Response resposta = target
//		"lo		.path("autenticar")
//				.request()
//				.post(Entity.entity(u, MediaTylifesgoodpe.APPLICATION_JSON_TYPE));
//		Usuario usuario = getEntity(resposta);
		return usuario;
	}

	public List<Usuario> pesquisarPorNome(String nome) {
//		Response resposta = target
//				.queryParam("nome", nome)
//				.request().get();
//		return getEntityList(resposta);
		return (List<Usuario>) restConector.getForObject(url, Usuario[].class);
	}

	public Usuario pesquisarPorId(long id) {
//		Response resposta = target
//				.path("id")
//				.path(String.valueOf(id))
//				.request().get();
//		return getEntity(resposta);
		Usuario usuario = restConector.getForObject(url+"/id/"+id);
		return usuario;
	}
}
