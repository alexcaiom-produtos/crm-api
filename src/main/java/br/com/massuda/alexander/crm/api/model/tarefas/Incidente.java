/**
 * 
 */
package br.com.massuda.alexander.crm.api.model.tarefas;

/**
 * @author Alex
 *
 */
public class Incidente extends Chamado {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1276636917360495635L;
	private Severidade severidade;
	private StatusIncidente status;

	public Severidade getSeveridade() {
		return severidade;
	}

	public void setSeveridade(Severidade severidade) {
		this.severidade = severidade;
	}

	public StatusIncidente getStatus() {
		return status;
	}

	public void setStatus(StatusIncidente status) {
		this.status = status;
	}

}
