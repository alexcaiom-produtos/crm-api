/**
 * 
 */
package br.com.massuda.alexander.crm.api.framework.model;

import java.util.Calendar;

import br.com.massuda.alexander.crm.api.model.Usuario;

/**
 * @author Alex
 *
 */
public class SessaoDeUsuario {

	private Usuario usuario;
	private Calendar horarioLogin;
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Calendar getHorarioLogin() {
		return horarioLogin;
	}
	public void setHorarioLogin(Calendar horarioLogin) {
		this.horarioLogin = horarioLogin;
	}
	
}
