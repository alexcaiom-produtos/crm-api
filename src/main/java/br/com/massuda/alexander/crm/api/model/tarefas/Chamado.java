/**
 * 
 */
package br.com.massuda.alexander.crm.api.model.tarefas;

import java.time.LocalDateTime;

import br.com.massuda.alexander.crm.api.model.Usuario;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.ChaveEstrangeira;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;

/**
 * @author Alex
 *
 */
public class Chamado extends EntidadeModelo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8896947527533864159L;
	String titulo;
	String descricao;
	LocalDateTime criacao = LocalDateTime.now();
	LocalDateTime resolucao;
	@ChaveEstrangeira(estaEmOutraTabela=true)
	Usuario responsavel;
	@ChaveEstrangeira(estaEmOutraTabela=true)
	Usuario criador;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public LocalDateTime getCriacao() {
		return criacao;
	}
	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}
	public LocalDateTime getResolucao() {
		return resolucao;
	}
	public void setResolucao(LocalDateTime resolucao) {
		this.resolucao = resolucao;
	}
	public Usuario getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}
	public Usuario getCriador() {
		return criador;
	}
	public void setCriador(Usuario criador) {
		this.criador = criador;
	}

}
