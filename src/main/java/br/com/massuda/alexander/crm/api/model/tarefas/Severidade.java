/**
 * 
 */
package br.com.massuda.alexander.crm.api.model.tarefas;

import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;

/**
 * @author Alex
 *
 */
public class Severidade extends EntidadeModelo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3061692760898700717L;
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
