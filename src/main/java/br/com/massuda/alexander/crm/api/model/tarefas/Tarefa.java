/**
 * 
 */
package br.com.massuda.alexander.crm.api.model.tarefas;

import javax.validation.constraints.NotNull;

/**
 * @author Alex
 *
 */
public class Tarefa extends Chamado {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7839696115267023926L;
	@NotNull
	private Prioridade prioridade;
	private StatusTarefa status = StatusTarefa.ABERTO;

	public Prioridade getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(Prioridade prioridade) {
		this.prioridade = prioridade;
	}

	public StatusTarefa getStatus() {
		return status;
	}

	public void setStatus(StatusTarefa status) {
		this.status = status;
	}
	
}
