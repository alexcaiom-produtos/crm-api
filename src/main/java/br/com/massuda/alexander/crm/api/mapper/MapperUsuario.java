/**
 * 
 */
package br.com.massuda.alexander.crm.api.mapper;

import org.springframework.util.ObjectUtils;

import br.com.massuda.alexander.crm.api.model.Usuario;

/**
 * @author Alex
 *
 */
public class MapperUsuario {

	public static Usuario to(br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario origem) {
		Usuario usuario = new Usuario();
		usuario.setId(origem.getId());
		usuario.setLogin(origem.getLogin());
		return usuario;
	}

	public static Usuario from(br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario origem) {
		Usuario destino = null;
		if (!ObjectUtils.isEmpty(origem)) {
			destino = new Usuario();
			destino.setId(origem.getId());
			destino.setLogin(origem.getLogin());
			destino.setNome(origem.getNome());
		}
		return destino;
	}

}
