/**
 * 
 */
package br.com.massuda.alexander.crm.api.model;

import br.com.massuda.alexander.crm.api.enumeration.StatusLogin;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.usuario.orm.modelo.Pessoa;

/**
 * @author Alex
 *
 */
@Tabela(criar=false)
public class Usuario extends Pessoa {

	private String login;
	private StatusLogin status;
	
	public Usuario() {
		
	}
	
	public Usuario (br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario usuarioModelo) {
		this.login = usuarioModelo.getLogin();
		this.status = StatusLogin.NORMAL;
	}
	
	public Usuario (br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario usuarioModelo, StatusLogin status) {
		this.login = usuarioModelo.getLogin();
		this.status = status;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public StatusLogin getStatus() {
		return status;
	}
	public void setStatus(StatusLogin status) {
		this.status = status;
	}
	
}
