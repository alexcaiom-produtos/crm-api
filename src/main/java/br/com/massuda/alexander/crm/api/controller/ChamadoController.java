/**
 * 
 */
package br.com.massuda.alexander.crm.api.controller;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.crm.api.dao.DAO;
import br.com.massuda.alexander.crm.api.dao.Finder;
import br.com.massuda.alexander.crm.api.mapper.MapperUsuario;
import br.com.massuda.alexander.crm.api.model.tarefas.Incidente;
import br.com.massuda.alexander.crm.api.model.tarefas.Prioridade;
import br.com.massuda.alexander.crm.api.model.tarefas.Tarefa;
import br.com.massuda.alexander.crm.api.servico.ServicoUsuario;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;
import br.com.waiso.framework.exceptions.ErroUsuario;

/**
 * @author Alex
 *
 */
@Controller
@RequestMapping("/chamado")
public class ChamadoController {
	
	@Autowired
	ServicoUsuario servicoUsuario;
	
	@PostMapping("/tarefa")
	@ResponseStatus(code = HttpStatus.OK)
	public void incluirTarefa(@Valid @RequestBody Tarefa tarefa) throws ErroUsuario, SQLException {
		DAO<Tarefa> dao = new DAO<Tarefa>(Tarefa.class) {};
		
		Usuario criador = servicoUsuario.pesquisarPorId(tarefa.getCriador().getId());//servicoUsuario.pesquisarPorLogin(tarefa.getCriador().getLogin());
		Usuario responsavel = servicoUsuario.pesquisarPorId(tarefa.getResponsavel().getId());//servicoUsuario.pesquisarPorLogin(tarefa.getCriador().getLogin());
		tarefa.setCriador(MapperUsuario.to(criador));
		tarefa.setResponsavel(MapperUsuario.to(responsavel));
		tarefa.setPrioridade(new Finder<>(Prioridade.class).pesquisar(tarefa.getPrioridade().getId()));
		
		dao.incluir(tarefa);
	}
	
	@PutMapping("/tarefa")
	@ResponseStatus(code = HttpStatus.OK)
	public void editarTarefa(@Valid @RequestBody Tarefa o) throws ErroUsuario, SQLException {
		DAO<Tarefa> dao = new DAO<Tarefa>(Tarefa.class) {};
		
		Usuario criador = servicoUsuario.pesquisarPorId(o.getCriador().getId());//servicoUsuario.pesquisarPorLogin(tarefa.getCriador().getLogin());
		Usuario responsavel = servicoUsuario.pesquisarPorId(o.getResponsavel().getId());//servicoUsuario.pesquisarPorLogin(tarefa.getCriador().getLogin());
		o.setCriador(MapperUsuario.to(criador));
		o.setResponsavel(MapperUsuario.to(responsavel));
		o.setPrioridade(new Finder<>(Prioridade.class).pesquisar(o.getPrioridade().getId()));
		
		dao.editar(o);
	}
	
	@ResponseBody
	@GetMapping("/tarefa")
	public List<Tarefa> listarTarefas() {
		Finder<Tarefa> finder = new Finder<Tarefa>(Tarefa.class) {};
		
		List<Tarefa> lista = finder.listar();
		for (int indice = 0; indice < lista.size(); indice++) {
			Usuario criador = servicoUsuario.pesquisarPorId(lista.get(indice).getCriador().getId());
			Usuario responsavel = null;
			if (ObjectUtils.isEmpty(lista.get(indice).getResponsavel())) {
				responsavel = servicoUsuario.pesquisarPorId(lista.get(indice).getResponsavel().getId());
			}
			lista.get(indice).setCriador(MapperUsuario.from(criador));
			lista.get(indice).setResponsavel(MapperUsuario.from(responsavel));
		}
		
		return lista;
	}
	
	@ResponseBody
	@GetMapping("/tarefa/{id}")
	public Tarefa pesquisarTarefa(@PathVariable long id) {
		Finder<Tarefa> finder = new Finder<Tarefa>(Tarefa.class) {};
		Tarefa tarefa = finder.pesquisar(id);
		br.com.massuda.alexander.crm.api.model.Usuario criador = MapperUsuario.from(servicoUsuario.pesquisarPorId(tarefa.getCriador().getId()));
		br.com.massuda.alexander.crm.api.model.Usuario responsavel = MapperUsuario.from(servicoUsuario.pesquisarPorId(tarefa.getResponsavel().getId()));
		
		tarefa.setCriador(criador);
		tarefa.setResponsavel(responsavel);
		return tarefa;
	}
	
	@ResponseBody
	@GetMapping("/tarefa/usuario/{usuario}")
	public List<Tarefa> listarTarefa(@PathVariable long usuario, LocalDateTime inicio, LocalDateTime fim) {
		Finder<Tarefa> finder = new Finder<Tarefa>(Tarefa.class) {};
		return finder.listar();
	}
	
	@PostMapping("/incidente")
	public void incluirIncidente(@RequestBody Incidente tarefa) throws ErroUsuario, SQLException {
		DAO<Incidente> dao = new DAO<Incidente>(Incidente.class) {};
		dao.incluir(tarefa);
	}
	
	@ResponseBody
	@GetMapping("/incidente/{id}")
	public Incidente pesquisarIncidente(@PathVariable long id) {
		Finder<Incidente> finder = new Finder<Incidente>(Incidente.class) {};
		return finder.pesquisar(id);
	}
	
	@ResponseBody
	@GetMapping("/incidente/{usuario}")
	public List<Incidente> listarIncidente(@PathVariable long usuario, LocalDateTime inicio, LocalDateTime fim) {
		Finder<Incidente> finder = new Finder<Incidente>(Incidente.class) {};
		return finder.listar();
	}

}
