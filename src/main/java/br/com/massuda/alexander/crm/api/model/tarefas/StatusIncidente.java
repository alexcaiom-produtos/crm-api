/**
 * 
 */
package br.com.massuda.alexander.crm.api.model.tarefas;

/**
 * @author Alex
 *
 */
public enum StatusIncidente {

	RESOLVIDO(null),
	VERIFICAR(RESOLVIDO),
	EM_ANDAMENTO(VERIFICAR),
	PENDENTE(EM_ANDAMENTO);
	
	private StatusIncidente proximo;

	private StatusIncidente(StatusIncidente proximo) {
		this.proximo = proximo;
	}

	public StatusIncidente getProximo() {
		return proximo;
	}
	
}
